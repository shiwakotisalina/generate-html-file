public class TableTags {

    public String getHead() {
        return htmlImpl.open("head");
    }
    public String getHeadClose() {
        return htmlImpl.close("head");
    }

    public String getHtml() {
        return htmlImpl.open("html");

    }
    public String getHtmlClose() {
        return htmlImpl.close("html");

    }

    public String getStyle() {
        return  htmlImpl.open("style");
    }
    public String getStyleClose() {
        return  htmlImpl.close("style");
    }

    public String getBody() {
        return htmlImpl.open("body");

    }

    public String getTable() {
       return  htmlImpl.open("table");
    }
    public String getTableClose() {
        return  htmlImpl.close("table");
    }

    public String getH1() {
        return htmlImpl.open("h1");
    }
    public String getH1Close() {
        return htmlImpl.close("h1");
    }

    public String getP1() {
        return htmlImpl.open("p1");
    }
    public String getP1Close() {
        return htmlImpl.close("p1");
    }


    public String getTr() {
        return  htmlImpl.open("tr");
    }
    public String getTrClose() {
        return  htmlImpl.close("tr");
    }

    public String getTh() {
        return  htmlImpl.open("th");
    }
    public String getThClose() {
        return  htmlImpl.close("th");
    }

    public String getTd() {
        return  htmlImpl.open("td");
    }
    public String getTdClose() {
        return  htmlImpl.close("td");
    }

    String html="html ";
    String head="head";
    String style="style";
    String body="body";
    String table="table";
    String h1="h1";
    String p1="p1";
    String tr="tr";
    String th="th";
    String td="td";

    HtmlImpl htmlImpl=new HtmlImpl();




}
