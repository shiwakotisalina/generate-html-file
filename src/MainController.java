import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

public class MainController {


    public static void main(String[] args) {

        String tt = "";


        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/customer_info", "root", "root");
//here sonoo is database name, root is username and password
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from information");
            System.out.println("id  first_name    last_name  phone  address");

            TableTags tableTags = new TableTags();


            while (rs.next()) {


                 tt += tableTags.getTr() + tableTags.getTd() + rs.getInt("id") + tableTags.getTdClose() + tableTags.getTd() + rs.getString("first_name") + tableTags.getTdClose() + tableTags.getTd() + rs.getString("last_name") +
                        tableTags.getTdClose() + tableTags.getTd() + rs.getString("phone") + tableTags.getTdClose() + tableTags.getTd() + rs.getString("address") + tableTags.getTdClose() + tableTags.getTrClose();

                System.out.println(rs.getInt("id") + "  " + rs.getString("first_name") + " " + rs.getString("last_name") + "" + rs.getString("phone") + "" + rs.getString("address"));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }


        String data = component(tt);

        File file = new File("C:\\backend\\generate html file/index.html");
        try {
            BufferedWriter rd = new BufferedWriter(new FileWriter(file));

            rd.write(data);


            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * <table>
     * <tr>
     * <th>Company</th>
     * <th>Contact</th>
     * <th>Country</th>
     * </tr>
     * <tr>
     * <td>Alfreds Futterkiste</td>
     * <td>Maria Anders</td>
     * <td>Germany</td>
     * </tr>
     * <tr>
     * <td>Centro comercial Moctezuma</td>
     * <td>Francisco Chang</td>
     * <td>Mexico</td>
     * </tr>
     * <p>
     * </table>
     *
     * @return
     */
    public static String component(String t) {
        String compo = "";
        HtmlImpl html = new HtmlImpl();
        TableTags tableTags = new TableTags();

        compo += html.open("html");
        compo += html.open("head") + html.open("title") + "Html Generator" + html.close("title") + html.close("head");
        compo += html.open("body") + html.open("h1") + "About" + html.close("h1") + html.open("textarea placeholder='Message'") + html.close("textarea");
        compo += html.open("br") + html.open("button type='submit'") + "submit" + html.close("button")+html.open("br")+html.close("br")

                + tableTags.getStyle() + "table , th , td {border:1px solid black}" + tableTags.getStyleClose() + html.open("table style ='width:100%'") + tableTags.getTr() + tableTags.getTh() + "id" + tableTags.getThClose()
                + tableTags.getTh() + "First Name" + tableTags.getThClose() + tableTags.getTh() + "Last Name" + tableTags.getThClose() + tableTags.getTh() + "phone" + tableTags.getThClose()
                + tableTags.getTh() + "address" + tableTags.getThClose() + tableTags.getTrClose() + t

                + html.close("body") + html.close("html");


        return compo;


//        String[] elements = {"html", "head", "title", "input", "textbox", "button"};
//        HtmlImpl html = new HtmlImpl();
//        for (int i = 0; i < elements.length; i++) {
//            compo = compo + html.open(elements[i]);
//            compo += html.close(elements[i]);
//            compo += "\n";
//        }
//        System.out.println("Data; "+compo);
//        return compo;
    }
}